#!/usr/bin/env bash

# set shell settings (see https://sipb.mit.edu/doc/safe-shell/)
set -euv -o pipefail

# create tarball of all artifacts
cd ./artifacts
tar -cf artifacts.tar *
# create digital signature of artifacts tarball
openssl dgst -sha256 -sign <(echo -e "$UPLOAD_SIGN_KEY") -out ./artifacts.tar.sha256 ./artifacts.tar
# create archive containing the artifacts and its digital signature
tar -cjf "./master.tbz2" ./artifacts.tar.sha256 ./artifacts.tar
# upload archive
curl --fail --basic -u "$UPLOAD_USER:$UPLOAD_PASS" -T "./master.tbz2" "$UPLOAD_URL/master.tbz2"
