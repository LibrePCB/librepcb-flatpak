FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
        curl \
        flatpak \
        flatpak-builder \
        git \
    && apt-get clean

RUN git config --global protocol.file.allow always
RUN flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
RUN flatpak install -y \
        flathub org.kde.Platform//5.15-22.08 \
        flathub org.kde.Sdk//5.15-22.08
